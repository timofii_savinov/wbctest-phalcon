<div class="container">
    {{form("index/signup")}}
        <h2 class="form-signin-heading">User Sign-Up</h2>
        <label for="inputName" class="sr-only" >Full name</label>
        <input type="text" class="form-control" name="name" placeholder="Full name">
        <br>
        <label for="inputEMail" class="sr-only" >Email address</label>
        <input type="email" class="form-control" name="email" placeholder="Email address">
        <br>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="text" class="form-control" name="password" placeholder="Password">
        <br>
        <label for="user_role" class="sr-only">User Role</label>
        <select class="form-control" name="role" id="user_role">
            <option value="">Select user role</option>
            <option value="1">User</option>
            <option value="2">Admin</option>
        </select>
        <br>
        <button class="btn btn-lg btn-primary btn-block" type="submit">User Sign-Up</button>
    </form>
</div>
