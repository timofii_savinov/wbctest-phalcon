<?php
declare(strict_types=1);

class AdminController extends ControllerBase
{

    public function indexAction()
    {
        if($this->session->get("AUTH_ROLE") != 2)
            $this->response->redirect('/');
    }
}

