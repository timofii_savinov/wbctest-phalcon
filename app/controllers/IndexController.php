<?php
declare(strict_types=1);

class IndexController extends ControllerBase
{

    public function indexAction()
    {

    }

    /*
     * create login page
     */
    public function loginAction(){

        if($this->request->isPost()){

            $dataSend = $this->request->getPost();
            $email = $dataSend['email'];
            $password = md5($dataSend['password']);

            $user = Users::findFirst([
                'conditions' => 'email = ?1 and password = ?2',
                'bind' => [
                    1 => $email,
                    2 => $password
                ]
            ]);

            if($user){
                if($user->active != 1){
                    echo "User Disable";
                    $this->view->disable();
                }else{

                    $this->session->set("AUTH_ID",$user->id);
                    $this->session->set("AUTH_NAME",$user->name);
                    $this->session->set("AUTH_EMAIL",$user->email);
                    $this->session->set("AUTH_ROLE",$user->role);
                    $this->session->set("AUTH_CREATED",$user->created);
                    $this->session->set("AUTH_UPDATED",$user->updated);
                    $this->session->set("IS_LOGIN", 1);


                    if($user->role === "1"){
                       return $this->response->redirect('/');
                    }else if($user->role === "2"){
                        return $this->response->redirect('admin/');
                    }else{
                        //exit;
                    }
                }
            }else{
                echo "Invalid Email and Password";
                $this->view->disable();
            }
        }
    }

    /*
     * create signup page
     */
    public function signupAction(){

        if($this->request->isPost()){

            $user = new Users();

            $user->setName($this->request->getPost('name'));
            $user->setEmail($this->request->getPost('email'));
            $user->setRole($this->request->getPost('role'));
            $user->setPassword(md5($this->request->getPost('password')));
            $user->setActive(1);
            $user->setCreated(time());
            $user->setUpdated(time());

            $output = $user->save();

            if($output){
                echo "Register is success!";
                $this->view->disable();
            }else{
                $messages = $user->getMessage();

                foreach ($messages as $message) {
                    echo $message->getMessage()."<br>";
                }
                $this->view->disabled();
            }
        }
    }

}

