<div class="container">
    <?= $this->tag->form(['index/login']) ?>
        <h2 class="form-signin-heading">User Login</h2>
        <label for="inputEMail" class="sr-only" >Email address</label>
        <input type="email" class="form-control" name="email" placeholder="Email address">
        <br>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="text" class="form-control" name="password" placeholder="Password">
        <br>
        <button class="btn btn-lg btn-primary btn-block" type="submit">User Login</button>
    </form>
</div>

